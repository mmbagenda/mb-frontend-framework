var APP = APP || {};

APP.common = APP.common || (function () {

	'use strict';

	function init() {
		/* code goes here */
	}

	return {
		init: init
	};
}());


$(document).ready(function () {
	'use strict';
	APP.common.init();
	APP.mobileNavigation.init();
});