var APP = APP || {};

// Mobile navigation - This will only work for 2 navigation tiers
APP.mobileNavigation =  (function () {

	'use strict';

	var $menuToggle,
		$menu,
		$menuItem,
		$menuActive,
		isMobileLayout,
		options,
		defaults = {
			speed: 250,
			headerLinks: false // If false and if top level item has children, this will be disabled on first click, allowing its children to display. Second click will navigate.
		};


	function menuOpen($el, speed) {
		var duration = options.speed;
		if (typeof speed === 'number') {
			duration = speed;
		}
		$menu.slideDown(duration);
		$el.addClass('expanded');
	}


	function menuClose($el, speed) {
		var duration = options.speed;
		if (typeof speed === 'number') {
			duration = speed;
		}
		$menu.slideUp(duration);
		$el.removeClass('expanded');
	}


	function toggleMenuExpand(evt) {
		/* jshint validthis: true */
		var $this;

		evt.preventDefault();

		$this = $(this);
		if ($this.hasClass('expanded')) {
			menuClose($this);
		} else {
			menuOpen($this);
		}
	}


	function handleMenuItemClick(evt) {
		/* jshint validthis: true */
		var $this;

		if ($menuToggle.is(':visible') === false) {
			return;
		}

		$this = $(this);
		
		// Slide up other open child UL's
		$('li.expanded').not(this).removeClass('link-active expanded').children('ul').slideUp(options.speed);
		
		// if children and not active
		if ($this.children('ul').children('li').length > 0 && !$this.hasClass('link-active')) {
			evt.preventDefault(); // disable navigation
		}
		
		// if click on arrow, not a link
		if ($this.hasClass('expanded')) {
			if (evt.target.nodeName.toUpperCase() === 'LI') {
				// Hide children
				$this.removeClass('link-active expanded').children('ul').slideUp(options.speed);
			}
		} else {
			// Show children
			$this.addClass('link-active expanded').children('ul').slideDown(options.speed);
		}
	}


	function handleResize() {
		// On resize hide any children
		if ($menuToggle.is(':visible') === false) {
			// Hide nav if NOT mobile and remove classes
			$('li.expanded').removeClass('expanded link-active')
				.children('ul')
				.hide()
				.css({'display': ''});
			// show top level nav if more
			$menu.show();
			//show active second level
			$menuActive.show();
			isMobileLayout = false;
		} else if (isMobileLayout === false) {
			// only close the menu if we weren't in mobile layout previously
			menuClose($menuToggle, 0);
			isMobileLayout = true;
		}
	}


	function init(initObj) {
		$menuToggle = $('#mobile-navigation-toggle');
		$menu = $('.navigation.primary > ul');
		$menuItem = $('.navigation.primary > ul > li') || $('.navigation.primary > ul > li a');
		$menuActive = $('.navigation.primary > ul li.active > ul');
		options = $.extend(defaults, initObj);

		// add class to list items with children
		$menuItem.filter('li').has('ul').addClass('has-children');

		// show / hide top level mobile nav
		$menuToggle.on('click', toggleMenuExpand);

		$(window).on('resize', handleResize);
		handleResize();
		
		// Show/hide second level
		if (options.headerLinks === false) {
			$menuItem.on('click', handleMenuItemClick);
		}

	}


	return {
		init: init
	};

} ());